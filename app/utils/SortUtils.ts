import {SortDirection} from "@/utils/SortDirection";

const sortByPropertyComparator = (key) => {
    return (a, b) => (a[key] > b[key]) ? 1 : ((b[key] > a[key]) ? -1 : 0);
};

export function sortBy(items, key, direction) {
    let sorted = items.sort(sortByPropertyComparator(key));

    if (direction == SortDirection.DESC) {
        sorted.reverse();
    }

    return sorted;
}