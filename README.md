# vue-nativescript-todo

## Usage

``` bash
# Install dependencies
npm install

# Preview on device
tns preview

# Build, watch for changes and run the application
tns run

# Build, watch for changes and debug the application
tns debug <platform>

# Build for production
tns build <platform> --env.production
```


## Install vue nativescript

https://nativescript-vue.org/ru/docs/getting-started/installation/

##Challenge

Create TODO application with realtime database. User should be able to perform basic CRUD operations with todo items. 

Only non empty todo item can be added to the list. 

Operations on specific todo item should satisfy following requirements:

1. by clicking on item checkbox todo should be marked as completed
2. by swiping item to the right delete option should occur. 

By clicking on delete option todo should be removed from the list by swiping item to the left modify option should occur. 
By clicking on modify option user should be able to perform text change and save or cancel todo modification

Todo items sorting and filtering will be considered as bonus functionalities to implement. 

Filtering should be implemented via search text bar. Sorter should include sortering based on date (asc/desc), alphabet (A-Z/Z-A), only complete items, only incomplete items. No third party libraries should be used.


Implementation requirements
Application should work both on Android and iOS platforms. Application should be covered with unit tests. Design is up to you.
